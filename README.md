# sbt

The interactive build tool, sbt is a build tool for Scala, Java, and more. [sbt](https://pkgs.alpinelinux.org/packages?name=sbt&arch=x86_64)

* [sbt new and Templates](https://www.scala-sbt.org/1.x/docs/sbt-new-and-Templates.html)
* Example use with SDKMAN https://gitlab.com/hub-docker-com-demo/sdkman